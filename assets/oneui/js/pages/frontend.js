/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */
var base_url = $('.base_url').val();
var FrontendFunction = function() {

    var initValidationLogin = function() {
        jQuery('.form-login').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 5,
                },
            },
            messages: {
                email: 'Please enter a valid email address',
                password: {
                    required: 'Please enter a password',
                    minlength: 'Your password must be at least 5 characters long',
                },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('Processing..');

                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        res = $.parseJSON(res);
                        if (res.data == '1') {
                            $('#modal-login').modal('hide');
                            $('.form-login')[0].reset();
                            window.location.reload();
                        }else {
                            alert(res.data);
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="fa fa-arrow-right push-5-r"></i> Log In');
                    },

                    error: function(jqXHR, exception) {
                       alert(jqXHR.statusText);
                      //   $('#modal-notif .block-content p').text(jqXHR.statusText);
                      //   $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.html('<i class="fa fa-arrow-right push-5-r"></i> Log In');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationRegister = function() {
        jQuery('.form-register').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                // ImageFile: {
                //     required: true,
                // },
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                phone: {
                    required: true,
                    number: true,
                },
                gender: {
                    required: true,
                },
                province: {
                    required: true,
                },
                city: {
                    required: true,
                },
                address: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 5,
                },
                // conf_pass: {
                //     minlength: 5,
                //     equalTo: '#password_update',
                // },
            },
            messages: {
                // ImageFile: {
                //     required: 'Please choose a picture',
                // },
                name: {
                    required: 'Please enter a name',
                },
                email: 'Please enter a valid email address',
                phone: {
                    required: 'Please enter a number phone',
                },
                gender: {
                    required: 'Please choose a gender',
                },
                province: {
                    required: 'Please choose a province',
                },
                city: {
                    required: 'Please choose a city',
                },
                address: {
                    required: 'Please enter a address',
                },
                password: {
                    required: 'Please enter a password',
                    minlength: 'Your password must be at least 5 characters long',
                },
                // conf_pass: {
                //     minlength: 'Your password must be at least 5 characters long',
                //     equalTo: 'Please enter the same password as above',
                // },
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('saving..');
                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        res = $.parseJSON(res);
                        if (res.data == '1') {
                            window.location.reload();
                            // $('#imgPreview').attr('src', base_url+'/assets/oneui/img/avatars/avatar1.jpg')
                        }else {
                            alert(res.data);
                        }
                        button.removeAttr('disabled');
                        button.text('Save Change');
                    },

                    error: function(jqXHR, exception) {
                       console.log(jqXHR)
                       alert(jqXHR.statusText);
                      //   $('#modal-notif .block-content p').text(jqXHR.statusText);
                      //   $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.text('Save Change');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };

    var initValidationForgot = function() {
        jQuery('.form-forgot').validate({
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },

            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('has-error');
                jQuery(e).closest('.help-block').remove();
            },

            rules: {
                email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                email: 'Please enter a valid email address',
            },
            submitHandler: function(form) {
                $form = $(form);
                var button = $form.find('button[type="submit"]');
                button.attr('disabled', 'disabled');
                button.text('Processing..');

                $.ajax({
                    url:$form.attr('action'),
                    type:'POST',
                    data:$form.serialize(),
                    success: function(res) {
                        res = $.parseJSON(res);
                        if (res.data == '1') {
                            $('#modal-forgot').modal('hide');
                            $('.form-forgot')[0].reset();
                            alert('Your password has been reset, please check your email.');
                            window.location.reload();
                        }else {
                            alert(res.data);
                        }
                        button.removeAttr('disabled');
                        button.html('<i class="fa fa-arrow-right push-5-r"></i> Log In');
                    },

                    error: function(jqXHR, exception) {
                       alert(jqXHR.statusText);
                      //   $('#modal-notif .block-content p').text(jqXHR.statusText);
                      //   $('#modal-notif').modal('show');
                        button.removeAttr('disabled');
                        button.html('<i class="fa fa-arrow-right push-5-r"></i> Log In');
                    },
                });
                return false; // required to block normal submit since you used ajax
            },
        });
    };


    return {
        init: function() {
            initValidationLogin();
            initValidationRegister();
            initValidationForgot();
        },
    };
}();

// Initialize when page loads
jQuery(function() {
    FrontendFunction.init();
});
$('.create-account').click(function(e){
  e.preventDefault();
  $('#modal-login').modal('hide');
  $('#city').attr('disabled','disabled');
  $.get(base_url+'jne/provinces', function(res){
      res = $.parseJSON(res);
      var opt = '<option></option>';
      $.each(res, function(i, v) {
          opt += '<option value="'+v.province_id+'">'+v.province+'</option>';
      })
      $('#province').html(opt);
      $('#modal-register').modal('show');
      setTimeout(function () {
        $('body').addClass('modal-open');
      }, 1000);

  });
});
$('#province').change(function(e){
    var province_id = $(this).val();
    $.get(base_url+'jne/cities/'+province_id, function(res){
        res = $.parseJSON(res);
        var opt = '';
        $.each(res, function(i, v) {
            opt += '<option value="'+v.city_id+'-'+v.city_name+'">'+v.city_name+'</option>';
        })
        $('#city').html(opt);
        $('#city').removeAttr('disabled');
    });

})
$('.login').click(function(e){
  e.preventDefault();
  $('#modal-register').modal('hide');
  $('#modal-forgot').modal('hide');
  $('#modal-login').modal('show');
  setTimeout(function () {
    $('body').addClass('modal-open');
  }, 1000);
});
$('.forgot-password').click(function(e){
  e.preventDefault();
  $('#modal-login').modal('hide');
  $('#modal-forgot').modal('show');
  setTimeout(function () {
    $('body').addClass('modal-open');
  }, 1000);
});

$('.form-register').submit(function(e){
  e.preventDefault();
  $.ajax({
      url: $(this).attr('action'),
      data: $(this).serialize(),
      type: 'POST',
      success: function(res){
        res = $.parseJSON(res);
        if (res.data == '1') {
            $('#modal-register').modal('hide');
            $('.form-register')[0].reset();
            window.location.reload();
        }else{
          alert(res.data);
        }
      },
      error: function(err){

      },
  })
});

$('.modal').on('hidden.bs.modal', function() {
    // $('body').addClass('modal-open');
});

var curr_url = window.location.href;
curr_url = curr_url.replace(/[0-9]/g, '');
$('.nav-main-header li a').each(function(){
   if($(this).attr('href') == curr_url){
      $('.nav-main-header li a').removeClass('active');
      $(this).addClass('active');
   }
});

var curr_url_cat = window.location.href;
$('.nav-pills > li:first').addClass('active');
$('.nav-pills > li').each(function(){
   if($(this).children('a').attr('href') == curr_url_cat){
      $('.nav-pills > li').removeClass('active');
      $(this).addClass('active');
   }
});
