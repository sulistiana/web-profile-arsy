<?php
class ProductCategoryModel extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_categories($where = null){
        $this->db->select('*');
        if ($where != null) {
            $this->db->where($where);
        }
        return $this->db->get('product_categories');
    }
    public function add_category($data = null){
        $result = false;
        if ($data != null) {
            $this->db->insert('product_categories', $data);
            $result = $this->db->insert_id();
        }
        return $result;
    }

    public function update_category($data = null, $where = null){
        $result = false;
        if($data != null && $where != null){
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update('product_categories');
        }
        return $result;
    }

    public function remove_category($where = null){
        if($where != null){
            return $this->db->delete('product_categories', $where);
        }
    }

}
