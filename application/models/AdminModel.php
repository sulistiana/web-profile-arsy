<?php
class AdminModel extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_admins($where = null){
        $this->db->select('id, photo, name, email, password');
        if ($where != null) {
            $this->db->where($where);
        }
        $this->db->order_by('id', 'desc');
        return $this->db->get('admins');
    }
    public function add_admin($data = null){
        $result = false;
        if ($data != null) {
            $this->db->insert('admins', $data);
            $result = $this->db->insert_id();
        }
        return $result;
    }

    public function update_admin($data = null, $where = null){
        $result = false;
        if($data != null && $where != null){
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update('admins');
        }
        return $result;
    }

    public function remove_admin($where = null){
        if($where != null){
            return $this->db->delete('admins', $where);
        }
    }

}
