<?php
class ProductModel extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_products($where = null){
        $this->db->select('*');
        if ($where != null) {
            $this->db->where($where);
        }
        $this->db->select('p.id, p.title, p.subtitle, p.image, pc.category_filter');
        $this->db->join('product_categories pc', 'p.product_category = pc.id');
        $this->db->order_by('p.id', 'desc');
        return $this->db->get('products p');
    }
    public function add_product($data = null){
        $result = false;
        if ($data != null) {
            $this->db->insert('products', $data);
            $result = $this->db->insert_id();
        }
        return $result;
    }

    public function update_product($data = null, $where = null){
        $result = false;
        if($data != null && $where != null){
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update('products');
        }
        return $result;
    }

    public function remove_product($where = null){
        if($where != null){
            return $this->db->delete('products', $where);
        }
    }

}
