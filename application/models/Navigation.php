<?php
class Navigation extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_navigation(){
        $data = array(
          'admin' => array(
              array(
                'url' => 'backend/dashboard',
                'displayName' => 'Dashboard',
                'child' => false,
                'heading' => false,
                'icon' => 'si-speedometer'
              ),
              array(
                'url' => '#',
                'displayName' => 'Internal Menu',
                'child' => false,
                'heading' => true,
                'icon' => ''
              ),
              array(
                'url' => 'backend/admins',
                'displayName' => 'Admin',
                'child' => false,
                'heading' => false,
                'icon' => 'si-user-following'
              ),
              array(
                'url' => '#',
                'displayName' => 'Apps Menu',
                'child' => false,
                'heading' => true,
                'icon' => ''
              ),
              array(
                'url' => '#',
                'displayName' => 'Products',
                'child' => array(
                  array(
                    'url' => 'backend/categories',
                    'displayName' => 'Product Categories'
                  ),
                  array(
                    'url' => 'backend/products',
                    'displayName' => 'Products'
                  ),
                ),
                'heading' => false,
                'icon' => 'si-ghost'
              ),
              array(
                'url' => 'backend/messages',
                'displayName' => 'Messages',
                'child' => false,
                'heading' => false,
                'icon' => 'si-envelope-letter'
              ),
            )//end array
        );
        return $data;
    }

}
