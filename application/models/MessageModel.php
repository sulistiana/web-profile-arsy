<?php
class MessageModel extends CI_Model {
    public function __construct(){
        parent::__construct();
    }
    public function get_messages($where = null){
        $this->db->select('*');
        if ($where != null) {
            $this->db->where($where);
        }
        $this->db->order_by('id', 'desc');
        return $this->db->get('messages');
    }
    public function add_message($data = null){
        $result = false;
        if ($data != null) {
            $this->db->insert('messages', $data);
            $result = $this->db->insert_id();
        }
        return $result;
    }

    public function update_message($data = null, $where = null){
        $result = false;
        if($data != null && $where != null){
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update('messages');
        }
        return $result;
    }

    public function remove_message($where = null){
        if($where != null){
            return $this->db->delete('messages', $where);
        }
    }

}
