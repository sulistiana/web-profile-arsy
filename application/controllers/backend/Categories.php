<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
		private $navigations;
		public function __construct(){
				// Call the CI_Controller constructor
				parent::__construct();
				$this->load->model('productCategoryModel');
				$this->load->model('navigation');
				$this->navigations = $this->navigation->get_navigation();

				$session = self::_is_logged_in();
				if(!$session) redirect('web-admin');
		}
		public function index(){
				// $this->load->view('welcome_message');
				$data['navigations'] = $this->navigations;
				$this->twig->display('backend/category', $data);
		}

		public function data($id = null){
				if ($id != null) {
						$where['id'] = $id;
						$category = $this->productCategoryModel->get_categories($where)->row();
						echo json_encode($category);
				}else{
		        $aColumns = array('id', 'category_name', 'category_filter');
						$sTable = 'product_categories';

		        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
		        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
		        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		        $iSortingCols = $this->input->get_post('iSortingCols', true);
		        $sSearch = $this->input->get_post('search', true);
		        $sEcho = $this->input->get_post('sEcho', true);

		        // Paging
		        if(isset($iDisplayStart) && $iDisplayLength != '-1'){
		            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		        }

		        // Ordering
		        if(isset($iSortCol_0)){
		            for($i=0; $i<intval($iSortingCols); $i++){
		                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
		                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
		                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);

		                if($bSortable == 'true')
		                {
		                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
		                }
		            }
		        }

		        /*
		         * Filtering
		         * NOTE this does not match the built-in DataTables filtering which does it
		         * word by word on any field. It's possible to do here, but concerned about efficiency
		         * on very large tables, and MySQL's regex functionality is very limited
		         */
		        if(isset($sSearch) && !empty($sSearch)){
		            for($i=0; $i<count($aColumns); $i++){
		                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);

		                // Individual column filtering
		                if(isset($bSearchable) && $bSearchable == 'true')
		                {
		                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
		                }
		            }
		        }

		        // Select Data
		        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
						$category = $this->session->userdata('user_data_category');
						$this->db->where('id !=', $category['id']);
						$rResult = $this->db->get($sTable);

		        // Data set length after filtering
		        $this->db->select('FOUND_ROWS() AS found_rows');
		        $iFilteredTotal = $this->db->get()->row()->found_rows;

		        // Total data set length
		        $iTotal = $this->db->count_all($sTable);

		        // Output
		        $output = array(
		            'sEcho' => intval($sEcho),
		            'iTotalRecords' => $iTotal,
		            'iTotalDisplayRecords' => $iFilteredTotal,
		            'aaData' => array()
		        );
		        foreach($rResult->result_array() as $aRow){
		            $row = array();

		            // foreach($aColumns as $col){
		            //     $row[$col] = $aRow[$col];
		            // }

		            $output['aaData'][] = $aRow;
		        }

		        echo json_encode($output);
				}
    }

		public function add(){
				$data = $this->input->post(null, true);
				$this->productCategoryModel->add_category($data);
				echo json_encode(array('data' => '1'));
		}

		public function update($id = null){
				$data = $this->input->post(null, true);
				$where['id'] = $id;
				$this->productCategoryModel->update_category($data, $where);
				echo json_encode(array('data' => '1'));
		}

		public function remove(){
				$category_id = $this->input->post('categoryId', true);
				foreach ($category_id as $id) {
						$where['id'] = $id;
						$this->productCategoryModel->remove_category($where);
				}
				echo json_encode(array('data' => '1'));
		}

		// HELPER FUNCTION

		private function _is_logged_in(){
        $category = $this->session->userdata('user_data_admin');
        return isset($category);
    }
}
