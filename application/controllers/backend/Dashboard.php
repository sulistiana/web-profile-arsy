<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
		private $navigations;
		public function __construct(){
				// Call the CI_Controller constructor
				parent::__construct();
				$this->load->model('navigation');
				$this->load->model('adminModel');
				$this->navigations = $this->navigation->get_navigation();
		}
		public function index(){
				$session = self::_is_logged_in();

				$data['navigations'] = $this->navigations;
				if(!$session) {
						$this->twig->display('backend/login', $data);
				}else {
						$this->twig->display('backend/dashboard', $data);
				}

		}

		// HELPER FUNCTION
		private function _is_logged_in(){
        $admin = $this->session->userdata('user_data_admin');
        return isset($admin);
    }
		private function _is_admin($where){
				$user = $this->adminModel->get_admins($where)->row_array();
        return $user;
    }
}
