<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {
		private $navigations;
		private $path_upload;
		public function __construct(){
				// Call the CI_Controller constructor
				parent::__construct();
				$this->load->library('bcrypt');
				$this->load->model('adminModel');
				$this->load->model('navigation');
				$this->navigations = $this->navigation->get_navigation();

				$this->path_upload = 'uploads/admins/';

				$func = $this->uri->segment(3);
				if($func != 'auth'){
						$session = self::_is_logged_in();
						if(!$session) redirect('mrbn-admin');
				}
		}
		public function index(){
				// $this->load->view('welcome_message');
				$data['navigations'] = $this->navigations;
				$this->twig->display('backend/admin', $data);
		}

		public function auth(){
				$data = $this->input->post(null, true);
				$where['email'] = $data['email'];
				$admin = $this->adminModel->get_admins($where)->row_array();
				if (!$admin){
					echo json_encode(array('data' => 'The email you entered does not belong to any account.'));
					return false;
				}
				if (!$this->bcrypt->check_password($data['password'], $admin['password'])){
					echo json_encode(array('data' => 'The password you entered is incorrect. Please enter again.'));
					return false;
				}
				unset($admin['password']);
				$this->session->set_userdata('user_data_admin', $admin);
				echo json_encode(array('data' => '1'));
		}
		public function logout(){
				// $this->session->sess_destroy();
				$this->session->unset_userdata('user_data_admin');
				redirect('/mrbn-admin');
		}

		public function data($id = null){
				if ($id != null) {
						$where['id'] = $id;
						$admin = $this->adminModel->get_admins($where)->row();
						echo json_encode($admin);
				}else{
		        $aColumns = array('id', 'photo', 'name', 'email');
						$sTable = 'admins';

		        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
		        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
		        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		        $iSortingCols = $this->input->get_post('iSortingCols', true);
		        $sSearch = $this->input->get_post('search', true);
		        $sEcho = $this->input->get_post('sEcho', true);

		        // Paging
		        if(isset($iDisplayStart) && $iDisplayLength != '-1'){
		            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		        }

		        // Ordering
		        if(isset($iSortCol_0)){
		            for($i=0; $i<intval($iSortingCols); $i++){
		                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
		                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
		                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);

		                if($bSortable == 'true')
		                {
		                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
		                }
		            }
		        }

		        /*
		         * Filtering
		         * NOTE this does not match the built-in DataTables filtering which does it
		         * word by word on any field. It's possible to do here, but concerned about efficiency
		         * on very large tables, and MySQL's regex functionality is very limited
		         */
		        if(isset($sSearch) && !empty($sSearch)){
		            for($i=0; $i<count($aColumns); $i++){
		                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);

		                // Individual column filtering
		                if(isset($bSearchable) && $bSearchable == 'true')
		                {
		                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
		                }
		            }
		        }

		        // Select Data
		        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
						$admin = $this->session->userdata('user_data_admin');
						$this->db->where('id !=', $admin['id']);
						$rResult = $this->db->get($sTable);

		        // Data set length after filtering
		        $this->db->select('FOUND_ROWS() AS found_rows');
		        $iFilteredTotal = $this->db->get()->row()->found_rows;

		        // Total data set length
		        $iTotal = $this->db->count_all($sTable);

		        // Output
		        $output = array(
		            'sEcho' => intval($sEcho),
		            'iTotalRecords' => $iTotal,
		            'iTotalDisplayRecords' => $iFilteredTotal,
		            'aaData' => array()
		        );
		        foreach($rResult->result_array() as $aRow){
		            $row = array();

		            // foreach($aColumns as $col){
		            //     $row[$col] = $aRow[$col];
		            // }

		            $output['aaData'][] = $aRow;
		        }

		        echo json_encode($output);
				}
    }

		public function add(){
				$img = $this->input->post('srcDataCrop');
				$name = self::_clean_text($_POST['name']);
				$_POST['photo'] = '';
				if($img) $_POST['photo'] = self::_upload_base64($img, $name);
				unset($_POST['srcDataCrop']);
				unset($_POST['repassword']);

				$password = $this->input->post('password', true);
				$_POST['password'] = $this->bcrypt->hash_password($password);

				$data = $this->input->post(null, true);
				$this->adminModel->add_admin($data);
				echo json_encode(array('data' => '1'));
		}

		public function update($id = null){
				// if($id == null) return echo "Invalid id";

				$password = $this->input->post('password', true);
				$_POST['password'] = $this->bcrypt->hash_password($password);

				if(!$password){
						unset($_POST['password']);
				}

				$img = $this->input->post('srcDataCrop');
				if($img){
						$name = self::_clean_text($_POST['name']);
						$_POST['photo'] = self::_upload_base64($img, $name, true, $id);
				}
				unset($_POST['srcDataCrop']);
				$data = $this->input->post(null, true);
				$where['id'] = $id;
				$this->adminModel->update_admin($data, $where);
				echo json_encode(array('data' => '1'));
		}

		public function remove(){
				$admin_id = $this->input->post('adminId', true);
				foreach ($admin_id as $id) {
						$where['id'] = $id;
						$admin = $this->adminModel->get_admins($where)->row();
						$curr_image = $this->path_upload.basename($admin->photo);
						if(file_exists($curr_image)){
								unlink($curr_image);
						}
						$where['id'] = $id;
						$this->adminModel->remove_admin($where);
				}
				echo json_encode(array('data' => '1'));
		}

		// HELPER FUNCTION
		private function _upload_base64($image = null, $name = null, $update = false, $id = null){
				$name_image = time().'_'.$name;

				$image = str_replace('data:image/png;base64,', '', $image);
				// $image = str_replace('data:image/png;base64,', '', $image);
				$image = str_replace(' ', '+', $image);
				$data = base64_decode($image);
				// $file = $this->path_upload.$name_image . '.jpg';
				$file = $this->path_upload.$name_image . '.png';
				$success = file_put_contents($file, $data);

				$url_image = base_url().$file;

				if($update && $id != null){
						$where['id'] = $id;
						$admin = $this->adminModel->get_admins($where)->row();
						$curr_image = $this->path_upload.basename($admin->photo);
						if(file_exists($curr_image)){
								unlink($curr_image);
						}
				}
				return $url_image;
		}

		private function _clean_text($name = null){
			return str_replace(array(' ', '-'), '_', $name);
		}

		private function _is_logged_in(){
        $admin = $this->session->userdata('user_data_admin');
        return isset($admin);
    }
}
