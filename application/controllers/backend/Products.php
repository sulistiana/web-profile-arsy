<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
		private $navigations;
		private $path_upload;
		public function __construct(){
				// Call the CI_Controller constructor
				parent::__construct();
				$this->load->model('productCategoryModel');
				$this->load->model('productModel');
				$this->load->model('navigation');
				$this->navigations = $this->navigation->get_navigation();

				$this->path_upload = 'uploads/products/';

				$session = self::_is_logged_in();
				if(!$session) redirect('web-admin');
		}
		public function index(){
				$data['navigations'] = $this->navigations;
				$data['categories'] = $this->productCategoryModel->get_categories()->result_array();
				$this->twig->display('backend/product', $data);
		}

		public function data($id = null){
				if ($id != null) {
						$where['p.id'] = $id;
						$product = $this->productModel->get_products($where)->row();
						echo json_encode($product);
				}else{
		        $aColumns = array('p.id', 'p.title', 'p.subtitle', 'p.image', 'p.description', 'pc.category_name');
						$sTable = 'products p';

		        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
		        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
		        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
		        $iSortingCols = $this->input->get_post('iSortingCols', true);
		        $sSearch = $this->input->get_post('search', true);
		        $sEcho = $this->input->get_post('sEcho', true);

		        // Paging
		        if(isset($iDisplayStart) && $iDisplayLength != '-1'){
		            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
		        }

		        // Ordering
		        if(isset($iSortCol_0)){
		            for($i=0; $i<intval($iSortingCols); $i++){
		                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
		                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
		                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);

		                if($bSortable == 'true')
		                {
		                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
		                }
		            }
		        }

		        /*
		         * Filtering
		         * NOTE this does not match the built-in DataTables filtering which does it
		         * word by word on any field. It's possible to do here, but concerned about efficiency
		         * on very large tables, and MySQL's regex functionality is very limited
		         */
		        if(isset($sSearch) && !empty($sSearch)){
		            for($i=0; $i<count($aColumns); $i++){
		                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);

		                // Individual column filtering
		                if(isset($bSearchable) && $bSearchable == 'true')
		                {
		                    $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
		                }
		            }
		        }

		        // Select Data
		        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
						$product = $this->session->userdata('user_data_product');
						$this->db->join('product_categories pc', 'p.product_category = pc.id');
						$rResult = $this->db->get($sTable);

		        // Data set length after filtering
		        $this->db->select('FOUND_ROWS() AS found_rows');
		        $iFilteredTotal = $this->db->get()->row()->found_rows;

		        // Total data set length
		        $iTotal = $this->db->count_all($sTable);

		        // Output
		        $output = array(
		            'sEcho' => intval($sEcho),
		            'iTotalRecords' => $iTotal,
		            'iTotalDisplayRecords' => $iFilteredTotal,
		            'aaData' => array()
		        );
		        foreach($rResult->result_array() as $aRow){
		            $row = array();

		            // foreach($aColumns as $col){
		            //     $row[$col] = $aRow[$col];
		            // }

		            $output['aaData'][] = $aRow;
		        }

		        echo json_encode($output);
				}
    }

		public function add(){
				$img = $this->input->post('srcDataCrop');
				$title = self::_clean_text($_POST['title']);
				$_POST['image'] = '';
				if($img) $_POST['image'] = self::_upload_base64($img, $title);
				unset($_POST['srcDataCrop']);
				$data = $this->input->post(null, true);
				$this->productModel->add_product($data);
				echo json_encode(array('data' => '1'));
		}

		public function update($id = null){
				$img = $this->input->post('srcDataCrop');
				if($img){
						$title = self::_clean_text($_POST['title']);
						$_POST['image'] = self::_upload_base64($img, $title, true, $id);
				}
				unset($_POST['srcDataCrop']);
				$data = $this->input->post(null, true);
				$where['id'] = $id;
				$this->productModel->update_product($data, $where);
				echo json_encode(array('data' => '1'));
		}

		public function remove(){
				$product_id = $this->input->post('productId', true);
				foreach ($product_id as $id) {
						$where['id'] = $id;
						$product = $this->productModel->get_products($where)->row();
						$curr_image = $this->path_upload.basename($product->image);
						if(file_exists($curr_image)){
								unlink($curr_image);
						}
						$where['id'] = $id;
						$this->productModel->remove_product($where);
				}
				echo json_encode(array('data' => '1'));
		}

		public function fcm(){
			$server_key = 'AIzaSyAsLb1SCJt2xJwCr15HXZD0he8BaGSfTS8';
	    $client = new Client();
	    $client->setApiKey($server_key);
	    $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

	    $message = new Message();
	    $message->setPriority('high');
	    $message->addRecipient(new Topic('anshor'));
	    $message
	        ->setNotification(new Notification('some title', 'some body'))
	        ->setData(['key' => 'value'])
	    ;
	    $response = $client->send($message);
	    // var_dump($response->getStatusCode());
	    var_dump($response->getBody()->getContents());
		}

		// HELPER FUNCTION
		private function _upload_base64($image = null, $name = null, $update = false, $id = null){
				$name_image = $name.'_'.time();
				$name_image = strtolower($name_image);
				$image = str_replace('data:image/png;base64,', '', $image);
				// $image = str_replace('data:image/png;base64,', '', $image);
				$image = str_replace(' ', '+', $image);
				$data = base64_decode($image);
				// $file = $this->path_upload.$name_image . '.jpg';
				$file = $this->path_upload.$name_image . '.png';
				$success = file_put_contents($file, $data);

				$url_image = base_url().$file;

				if($update && $id != null){
						$where['id'] = $id;
						$product = $this->productModel->get_products($where)->row();
						$curr_image = $this->path_upload.basename($product->image);
						if(file_exists($curr_image)){
								unlink($curr_image);
						}
				}
				return $url_image;
		}

		private function _clean_text($name = null){
			return str_replace(array(' ', '-'), '_', $name);
		}

		private function _is_logged_in(){
        $product = $this->session->userdata('user_data_admin');
        return isset($product);
    }
}
