<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {
		public function __construct(){
				parent::__construct();
		}
		public function index(){
				$this->load->model('productCategoryModel');
				$this->load->model('productModel');
				$data['categories'] = $this->productCategoryModel->get_categories()->result_array();
				$data['products'] = $this->productModel->get_products()->result_array();
				$this->twig->display('frontend/app', $data);
		}

		public function detail($id){
				$this->load->model('productModel');
				$data['product'] = $this->productModel->get_products(array('p.id' => $id))->row_array();
				$this->twig->display('frontend/product', $data);
		}

		public function send(){
				$this->load->model('messageModel');
				$data = $this->input->post(null, true);
				$this->messageModel->add_message($data);
				echo json_encode(array('data' => '1'));
		}
}
