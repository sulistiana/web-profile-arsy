<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CartData {

		public function get_cart_data(){
        $ci =& get_instance();
        $data_cart['cart_contents'] = $ci->cart->contents();
        $data_cart['cart_total_amount'] = $ci->cart->total();
        $data_cart['cart_total_items'] = count($data_cart['cart_contents']);
        $ci->twig->addGlobal('data_cart', $data_cart);
		}
}
